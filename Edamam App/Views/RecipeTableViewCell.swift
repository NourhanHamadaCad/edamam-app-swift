//
//  RecipeTableViewCell.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//


import UIKit
import AlamofireImage

class RecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleRecipeLbl: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var sourceLbl: UILabel!
    @IBOutlet weak var healthyLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundBorder(view: recipeImage)
      
    }
    
    func roundBorder (view : UIView){
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 20
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
    }
    func setup(recipe: RecipePresentation) {
        self.titleRecipeLbl.text = recipe.title
        var healthyLabelStr = Constants.RecipeDetail.HealthyLables
        for label in recipe.healthyLabel {
            healthyLabelStr.append("  \(label)")
        }
        print("healthy labels are " , healthyLabelStr)
        self.healthyLbl.text = healthyLabelStr
        self.sourceLbl.text = recipe.source
        if let url = URL(string: recipe.imageURl) {
            print("image " , recipe.imageURl)
            self.recipeImage.af_setImage(withURL: url)
            UIView.animate(withDuration: 1.0) {
                self.layoutIfNeeded()
            }
        }
    }
}
