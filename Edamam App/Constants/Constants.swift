//
//  Constants.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import UIKit

///Contains app constants.
enum Constants {
    
    ///Alert error title.
    static var ErrorTitle:String {
        return "Ops!"
    }
    ///Alert actions
    enum Actions {
        static var Ok:String {
            return "OK"
        }
    }
    
    ///Keys for RecipeList scene.
    enum RecipeList {
        static var NoRecipes:String {
            return "No items to show."
        }
        
       
        
        ///Bar button titles
        enum BarButton {
            static var Search:String {
                return "Search"
            }
        }
        
        ///Error messages.
        enum Errors {
            static var NeedToEnterTitle:String {
                return "You must enter a title."
            }
            static var ValidYear:String {
                return "Please enter a valid year."
            }
            static var NoCellFound:String {
                return "Cell cannot be found!"
            }
        }
    }
    
    enum RecipeDetail {
        
        static var Publisher:String {
            return "Publisher is :\n"
        }
        
        static var HealthyLables:String {
                   return "Healthy Lables are :\n"
               }
               
               static var IngredientLines:String {
                   return "Ingredients are :\n"
               }
    }
    
}


enum Storyboard {
    static var Main:UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    enum Name {
        static var Main:String {
            return "Main"
        }
    }
}

enum Cell {
    enum NibName {
        static var Recipe:String{
            return "RecipeTableViewCell"
        }
    }
    enum Identifier {
        static var Recipe:String {
            return "RecipeTableViewCell"
        }
    }
}

enum ViewController {
    enum Identifier {
        static var RecipeList:String {
            return "RecipeListViewController"
        }
        static var RecipeDetail:String {
            return "RecipeDetailViewController"
            
        }
    }
}


