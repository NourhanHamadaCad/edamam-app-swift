//
//  AppRouter.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import UIKit

final class AppRouter {
    let window:UIWindow
    
    init() {
        window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    func start() {
        let recipesVC = RecipeListBuilder.make()
        let navigationController = UINavigationController(rootViewController: recipesVC)

        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
