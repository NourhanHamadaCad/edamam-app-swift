//
//  AppContainer.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//


import Foundation

let app = AppContainer()


///Contains common dependencies.
final class AppContainer {
    let router = AppRouter()
    let service = RecipeService()
}
