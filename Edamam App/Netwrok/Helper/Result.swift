//
//  Result.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}
