//
//  Router.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation
import Alamofire

///Enables type safe access to request parameters and methods.
public enum Router: URLRequestConvertible {
    
    enum Constants {
        static let baseURLPath = "https://api.edamam.com/search"
        static let apiKey = "b0c4f8afaa13f79ee3219775e0d90d23"
        static let appId = "89193820"

    }
    
    case recipe(title:String)
    case recipeDetails(id:String)

    var method:HTTPMethod {
        switch self {
        case .recipe, .recipeDetails:
            return .get
        }
    }
    
    var path: String {
        switch self {
        default:
            return ""
        }
    }
    
    var parameters:[String: Any] {
        var params = ["app_key" : Constants.apiKey as Any , "app_id" : Constants.appId as Any ]
        
        switch self {
        case .recipe(let title):
            params["q"] = title as Any
           
            
        case .recipeDetails(let id):
            params["i"] = id as Any
        }
        return params
    }
    
    public func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseURLPath.asURL()
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        print("url is " , request.url?.absoluteURL)
        return try URLEncoding.default.encode(request, with: parameters)
    }
}

