//
//  RecipeService.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation
import Alamofire

/// Protocol of the `RecipeService`.
protocol RecipeServiceProtocol {
    func fetchRecipes(title:String, completion:  @escaping (Result<RecipeListResponse>) -> Void)
    
}

/// Handles data requests to Imdb service.
class RecipeService: RecipeServiceProtocol {
    
    enum Error: Swift.Error {
        case serializationError(internal: Swift.Error)
        case networkError(internal: Swift.Error)
    }
    
    func fetchRecipes(title: String, completion: @escaping (Result<RecipeListResponse>) -> Void) {
        request(Router.recipe(title: title)).responseData { (response) in
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    print("urllll" , response.request?.url , response)
                    let response = try decoder.decode(RecipeListResponse.self, from: data)
                    completion(.success(response))
                }catch {
                    completion(.failure(Error.serializationError(internal: error)))
                }
            case .failure(let error):
                completion(.failure(Error.networkError(internal: error)))
            }
        }
    }
    
   
}


