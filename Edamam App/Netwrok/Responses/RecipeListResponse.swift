//
//  RecipeListResponse.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

///Response object that decodes the list of recipes fetched from the OMdb.
struct RecipeListResponse: Decodable {
    private enum RootCodingKeys: String, CodingKey {
        case search = "hits"
    }

    let recipes: [Recipes]
    
    init(recipes: [Recipes]) {
        self.recipes = recipes
    }
    
    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        self.recipes = try rootContainer.decode([Recipes].self, forKey: .search)
    }
    
}
