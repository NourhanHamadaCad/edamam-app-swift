//
//  RecipeListViewController.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import UIKit


final class RecipeListViewController: UIViewController, AlertDisplayer {
    
    @IBOutlet weak var titleSearchBar: UISearchBar!
    
    @IBOutlet private weak var tableView: UITableView!

    var viewModel: RecipeListViewModelProtocol! {
        didSet {
            viewModel.delegate = self
        }
    }

    private var recipeList: [RecipePresentation] = []

    private enum LoadType {
        case loadFirst
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        view.accessibilityIdentifier = "recipeListView"
        self.tableView.register((UINib(nibName: Cell.NibName.Recipe, bundle: nil)), forCellReuseIdentifier: Cell.Identifier.Recipe)

        self.tableView.separatorStyle = .none
        let searchButton = UIBarButtonItem(title: Constants.RecipeList.BarButton.Search, style: .plain, target: self, action: #selector(searchButtonTapped))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.green
        navigationItem.rightBarButtonItem = searchButton
    }
    
    @objc func searchButtonTapped() {
 
        loadData(with: .loadFirst)
        
        self.resignFirstResponder()
        self.view.endEditing(true)
        
    }

    private func loadData(with loadAction: LoadType){
        guard let title = titleSearchBar.text else { return }
        let validation = viewModel.validateEntries(title: title)
        switch validation {
        case .valid:
            switch loadAction{
            case .loadFirst:
                viewModel.loadRecipes(for: title)
                
            }
       
        case .invalidTitle:
            let action = UIAlertAction(title: Constants.Actions.Ok, style: .default)
            displayAlert(with: Constants.ErrorTitle, message: Constants.RecipeList.Errors.NeedToEnterTitle, actions: [action])
        }
    }
    
}

//MARK: - RecipeListViewModelDelegate
extension RecipeListViewController: RecipeListViewModelDelegate {
    
    func handleViewModel(output: RecipeListViewModelOutput) {
        switch output {
        case .setLoading(let isLoading):
            UIApplication.shared.isNetworkActivityIndicatorVisible = isLoading
        case .showRecipeList(let recipeList):
            self.recipeList = recipeList
            if self.recipeList.count > 0 {
                self.tableView.separatorStyle = .singleLine
            }else{
                self.tableView.separatorStyle = .singleLine
                
            }
            tableView.reloadData()
        case .showEmptyList:
            self.recipeList = []
            self.tableView.separatorStyle = .none
            tableView.reloadData()
        }
    }
    
   
}

//MARK: - UITableViewDataSource
extension RecipeListViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.Identifier.Recipe, for: indexPath) as? RecipeTableViewCell else {fatalError(Constants.RecipeList.Errors.NoCellFound)}
        cell.setup(recipe: recipeList[indexPath.row] )
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeList.count ?? 10
    }
    
   
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.recipeList.count == 0 {
            return Constants.RecipeList.NoRecipes
        }else{
            
        }
        return nil
    }
}

//MARK: - UITableViewDelegate
extension RecipeListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let recipeDetails =  recipeList[indexPath.row]
        
        
        let vc = Storyboard.Main.instantiateViewController(withIdentifier: ViewController.Identifier.RecipeDetail) as! RecipeDetailViewController
        vc.recipeDetails = recipeDetails
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // return if another service is on call.
        guard !UIApplication.shared.isNetworkActivityIndicatorVisible else { return }
    }
}



//MARK: - UISearchBarDelegate
extension RecipeListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchButtonTapped()
        self.view.endEditing(true)
    }
    
}


