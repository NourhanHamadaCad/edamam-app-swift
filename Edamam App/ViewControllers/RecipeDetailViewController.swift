//
//  RecipeDetailViewController.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import UIKit
import AlamofireImage
import SafariServices


class RecipeDetailViewController: UIViewController, AlertDisplayer {
    
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var healthyLabelsLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UITextView!
    
    //default poster height is 0.
    @IBOutlet weak var posterHeightContraint: NSLayoutConstraint!
    private let posterHeight:CGFloat = 180.0
    
    var recipeDetails : RecipePresentation?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showDetail()
        let tap = UITapGestureRecognizer(target: self, action: #selector(RecipeDetailViewController.openPublisher))
               publisherLabel.isUserInteractionEnabled = true
               publisherLabel.addGestureRecognizer(tap)
    }


    
    func showDetail() {
        recipeTitle.text = recipeDetails?.title
        sourceLabel.text = recipeDetails?.source
        
        var healthyLabelStr = Constants.RecipeDetail.HealthyLables
        for label in recipeDetails!.healthyLabel {
            healthyLabelStr.append("  \(label)")
        }
        
        healthyLabelsLabel.text = healthyLabelStr
        
        publisherLabel.attributedText = NSAttributedString(string: "\(recipeDetails?.publisher ?? "")", attributes:
        [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
        
        var ingredientsStr = Constants.RecipeDetail.IngredientLines
        for label in recipeDetails!.ingerdeints {
            ingredientsStr.append("  \(label)")
        }
        ingredientsLabel.text = ingredientsStr

        guard recipeDetails?.imageURl != "N/A" else {return}
        
        if let url = URL(string: recipeDetails!.imageURl) {
            recipeImage.af_setImage(withURL: url)
            
            //animate poster height.
            posterHeightContraint.constant = posterHeight
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func openPublisher (){
           let svc = SFSafariViewController(url: NSURL(string: self.publisherLabel.text!)! as URL)
           self.present(svc, animated: true, completion: nil)
       }
}
