//
//  Recipes.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

/// The model that contains basic recipe information.
struct Recipes: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case recipe
       
       // case imdbID
    }
    
    var recipe:Recipe
  
}

struct Recipe: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case label
        case source
        case publisher = "url"
        case image
        case healthLabels
        case ingredientLines

    }
    
    var label:String
    var source:String
    var publisher:String
    var image:String
    var healthLabels:[String]
    var ingredientLines:[String]


}

