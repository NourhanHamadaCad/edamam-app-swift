//
//  RecipeListViewModel.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation
final class RecipeListViewModel: RecipeListViewModelProtocol {
    weak var delegate: RecipeListViewModelDelegate?
    private let service: RecipeServiceProtocol
    private var recipes: [Recipes] = []
    init(service: RecipeServiceProtocol) {
        self.service = service
    }
    
    private var nextPage = 1
    
    func loadRecipes(for title: String) {

        notify(.setLoading(true))
        service.fetchRecipes(title: title) { [weak self] (response) in
            guard let `self` = self else { return }
            self.notify(.setLoading(false))
            
            switch response {
            case .success(let result):
               
                    self.recipes = result.recipes
              
                
                    let presentations = self.recipes.map({ RecipePresentation(title: $0.recipe.label, source: $0.recipe.source, healthyLabel: $0.recipe.healthLabels , imageURl:  $0.recipe.image , ingerdeints:  $0.recipe.ingredientLines , publisher: $0.recipe.publisher)  })
                self.notify(.showRecipeList(presentations))
                
                print("result is " , response , result )
            case .failure(_):
                    self.notify(.showEmptyList)
            }
        }
    }
    
   
    
  
    
    func validateEntries(title: String) -> EntryValidation  {
        
        guard title != "" else {
            return .invalidTitle
        }
       
        return .valid
    }
    
    private func notify(_ output: RecipeListViewModelOutput) {
        delegate?.handleViewModel(output: output)
    }
    
}

enum EntryValidation {
    case valid
    case invalidTitle
}
