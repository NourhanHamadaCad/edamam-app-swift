//
//  String+Helper.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

extension String {
    
    ///Converts the strings with "N/A" to empty string "".
    func withoutNA() -> String {
        if self == "N/A" {
            return ""
        }
        return self
    }
}
