//
//  Array+Helper.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

extension Array {
    
    struct IndexOutOfBoundsError: Error { }
    
    func element(at index: Int) throws -> Element {
        guard index >= 0 && index < self.count else {
            throw IndexOutOfBoundsError()
        }
        return self[index]
    }
}
