//
//  RecipeListBuilder.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import UIKit

///Builds a `RecipeListViewController`, and injects dependencies.
final class RecipeListBuilder {
    
    static func make() -> RecipeListViewController {
        let viewController = Storyboard.Main.instantiateViewController(withIdentifier: ViewController.Identifier.RecipeList) as! RecipeListViewController
        viewController.viewModel = RecipeListViewModel(service: app.service)
        return viewController
    }
}
