//
//  RecipePresentation+API.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

extension RecipePresentation {
    //convenient initalizer is created to enable initializing the presentation object with a single parameter.
    convenience init(recipe: Recipes) {
        self.init(title: recipe.recipe.label, source: recipe.recipe.source, healthyLabel: recipe.recipe.healthLabels , imageURl : recipe.recipe.image , ingerdeints: recipe.recipe.ingredientLines , publisher: recipe.recipe.publisher)
    }
}
