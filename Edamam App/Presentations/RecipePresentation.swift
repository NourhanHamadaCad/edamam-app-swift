//
//  RecipePresentation.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

///Presentation of `Recipe` object.
final class RecipePresentation: NSObject {
    let title: String
    let source: String
    let healthyLabel: [String]
    let imageURl: String
    let ingerdeints: [String]
    let publisher: String

    init(title:String, source:String, healthyLabel:[String] , imageURl : String , ingerdeints: [String] , publisher: String) {
        self.title = title
        self.source = source
        self.healthyLabel = healthyLabel
        self.imageURl = imageURl
        self.ingerdeints = ingerdeints
        self.publisher = publisher

        super.init()
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? RecipePresentation else { return false }
        return self.title == other.title && self.healthyLabel == other.healthyLabel && self.source == other.source && self.imageURl == other.imageURl &&  self.ingerdeints == other.ingerdeints &&  self.publisher == other.publisher
    }
}

