//
//  RecipeListContracts.swift
//  Edamam App
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation

///The protocol of the RecipeListViewModel.
protocol RecipeListViewModelProtocol {
    var delegate:RecipeListViewModelDelegate? { get set }
    func loadRecipes(for title: String)
    func validateEntries(title:String) -> EntryValidation
}

///List of outputs that the viewModel sends to the viewController object.
enum RecipeListViewModelOutput: Equatable {
    case setLoading(Bool)
    case showRecipeList([RecipePresentation])
    case showEmptyList
}

/**
 Delegate of the RecipeListViewModel.
 # Conformes must comply listed features:
 - `handleViewModel(output)`
 */

protocol RecipeListViewModelDelegate: class {
    func handleViewModel(output: RecipeListViewModelOutput)
}


