//
//  MockRecipeService.swift
//  Edamam AppTests
//
//  Created by Nourhan Hamada on 6/24/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//


import Foundation
@testable import Edamam_App

final class MockRecipeService: RecipeServiceProtocol {

    var recipes: [Recipes] = []
    
    func fetchRecipes(title: String , completion: @escaping (Result<RecipeListResponse>) -> Void) {
        completion(.success(RecipeListResponse(recipes: recipes)))
    }
    
  
    
}
