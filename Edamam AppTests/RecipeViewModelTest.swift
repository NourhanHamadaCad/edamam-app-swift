//
//  RecipeViewModelTest.swift
//  Edamam AppTests
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import XCTest
@testable import Edamam_App

class RecipeViewModelTest: XCTestCase {
    
    private var viewModel: RecipeListViewModel!
    private var service: MockRecipeService!
    
    override func setUp() {
        service = MockRecipeService()
        viewModel = RecipeListViewModel(service: service)
    }
    
    
    func testLoadRecipes() throws {
        //Given:
        let recipe1 = try ResourceLoader.loadRecipe(resource: .recipe1)
        let recipe2 = try ResourceLoader.loadRecipe(resource: .recipe2)
        let recipe3 = try ResourceLoader.loadRecipe(resource: .recipe3)
        service.recipes = [recipe1, recipe2, recipe3]
        
        //When:
        viewModel.loadRecipes(for: "s")
        
        
    }
    
    func testNavigation() throws {
        // Given:
        let recipe1 = try ResourceLoader.loadRecipe(resource: .recipe1)
        let recipe2 = try ResourceLoader.loadRecipe(resource: .recipe2)
        let recipe3 = try ResourceLoader.loadRecipe(resource: .recipe3)

        service.recipes = [recipe1, recipe2, recipe3]
        viewModel.loadRecipes(for: recipe1.recipe.label)
        
        
        
        
    }
    
}


    
   

