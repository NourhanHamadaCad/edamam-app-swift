//
//  ResourceLoader.swift
//  Edamam AppTests
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import Foundation
import XCTest
@testable import Edamam_App

class ResourceLoader {
    
    enum RecipeResource: String {
        case recipe1
        case recipe2
        case recipe3
    }
    
  
    
    static func loadRecipe(resource: RecipeResource) throws -> Recipes {
        let bundle = Bundle.test
        guard let url = bundle.url(forResource: resource.rawValue, withExtension: "json") else {
            XCTFail("\(resource.rawValue) resource cannot be found.")
            return Recipes(recipe: Recipe(label: "", source: "", publisher: "", image: "", healthLabels: [], ingredientLines: []))
        }
        let data = try Data(contentsOf: url)
        let recipe = try JSONDecoder().decode(Recipes.self, from: data)
        return recipe
    }
    
   
}

private extension Bundle {
    class Dummy { }
    static let test = Bundle(for: Dummy.self)
}
