//
//  RecipeTest.swift
//  Edamam AppTests
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//


import XCTest

@testable import Edamam_App
class RecipeTest: XCTestCase {

    func testParsing () throws {
        let bundle = Bundle(for: RecipeTest.self)
        guard let url = bundle.url(forResource: "recipe1", withExtension: "json") else {
            XCTFail("recipe resouce cannot be found.")
            return
        }
        let data = try Data(contentsOf: url)
        let recipe = try JSONDecoder().decode(Recipes.self, from: data)
        print("recipe data is " , recipe)
        XCTAssertEqual(recipe.recipe.label, "Michelles Tomato Salad")
        XCTAssertEqual(recipe.recipe.source, "Food52")
        XCTAssertEqual(recipe.recipe.publisher, "https://food52.com/recipes/13138-michelle-s-tomato-salad")
    }
    
}
