//
//  RecipeListUITest.swift
//  Edamam AppUITests
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import XCTest

class RecipeListUITest: XCTestCase {

    var app:XCUIApplication!
    
    override func setUp() {

        super.setUp()
        
        continueAfterFailure = false

        app = XCUIApplication()
        
        XCUIApplication().launch()

    }


    func testRecipeListPage() {
        app.launch()
        XCTAssertTrue(app.isDisplayingRecipeList)
        
    }
    
}
