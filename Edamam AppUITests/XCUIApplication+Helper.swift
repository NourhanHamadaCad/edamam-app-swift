//
//  XCUIApplication+Helper.swift
//  Edamam AppUITests
//
//  Created by Nourhan Hamada on 6/26/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

import XCTest

extension XCUIApplication {
    var isDisplayingRecipeList: Bool {
        return otherElements["recipeListView"].exists
    }
}
